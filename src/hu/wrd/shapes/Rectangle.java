package hu.wrd.shapes;

public class Rectangle implements Shape {

    int x;
    int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    Rectangle(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double area(){
        double A = 0;
        A = x *y;
        return A;
    }
}
