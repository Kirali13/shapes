package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Shape> shapesList = new ArrayList<>();
        shapesList.add(new Square(1));
        shapesList.add(new Square(2));
        shapesList.add(new Square(3));
        shapesList.add(new Rectangle(2, 3));
        shapesList.add(new Rectangle(2, 4));
        shapesList.add(new Rectangle(3, 4));
        shapesList.add(new Rectangle(3, 5));
        shapesList.add(new Rectangle(4, 5));
        shapesList.add(new Circle(1));
        shapesList.add(new Circle(2));
        shapesList.add(new Circle(3));

        ShapeService shapeService = new ShapeService(shapesList);
        shapeService.addShapes(
                new Square(4),
                new Rectangle(3, 3),
                new Rectangle(0, 3),
                new Circle(4));

        for (int i = 0; i < shapesList.size() - 1; i++) {
            for (int j = i + 1; j < shapesList.size(); j++) {
                if (shapesList.get(i).area() == shapesList.get(j).area()) {
                    shapesList.remove(shapesList.get(i));
                }
            }
        }


        // Rectangle(3, 3) is the same shape as new Square(3) added previously, so none of these 3 shapes should be added.
        // Please handle this error without exiting the program
        /*shapeService.addShapes(
                new Square(4),
                new Rectangle(3, 3),
                new Circle(4));*/

        shapeService.printShapesOrderByAreaAsc();

        System.out.println("-----------------");

        shapeService.printShapesOrderByAreaDesc();

    }
}
