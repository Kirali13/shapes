package hu.wrd.shapes;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShapeService {

    private final List<Shape> shapeList;

    public ShapeService(List<Shape> shapeList) {
        this.shapeList = shapeList;
    }

    public void addShapes(Shape ... shapes) {
        for (Shape shape : shapes){
            if (shape.area() == 0 || shape.area() < 0){
                System.out.println("Wrong input!");
            }else {
                shapeList.add(shape);
            }
        }
    }

    public void printShapesOrderByAreaAsc() {
        shapeList.sort(Comparator.comparingDouble(Shape::area));
        for (Shape shape : shapeList) {
            System.out.println("Shape area: " + shape.area());
        }
        System.out.println("printShapesOrderByAreaAsc() called");
    }

    public void printShapesOrderByAreaDesc() {
        shapeList.sort(Comparator.comparingDouble(Shape::area).reversed());
        for (Shape shape : shapeList) {
            System.out.println("Shape area: " + shape.area());
        }
        System.out.println("printShapesOrderByAreaDesc() called");
    }
}
