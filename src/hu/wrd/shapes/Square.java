package hu.wrd.shapes;

public class Square implements Shape {
    int x;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    Square(int x) {
        this.x = x;
    }

    public double area(){
        double A = 0;
        A = x*x;
        return A;
    }
}
