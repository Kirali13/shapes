package hu.wrd.shapes;

public class Circle implements Shape {
    int radius;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    Circle(int radius) {
        this.radius = radius;
    }

    public double area(){
        double A = 0;
        A = Math.PI *(radius*radius);
        return A;
    }
}
